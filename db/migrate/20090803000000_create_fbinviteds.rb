class CreateFbinviteds < ActiveRecord::Migration
  def self.up
    create_table :fbinviteds, :force => true do |t|      
      t.integer :number_invited
      t.timestamps 
    end
  end

  def self.down
    drop_table :fbinviteds
  end
end
