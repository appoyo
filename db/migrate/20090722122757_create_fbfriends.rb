class CreateFbfriends < ActiveRecord::Migration
  def self.up
    create_table :fbfriends, :force => true do |t|      
      t.string :facebook_id, :null => false
      t.text :friends_list, :null => false
      t.timestamps 
    end
  end

  def self.down
    drop_table :fbfriends
  end
end
