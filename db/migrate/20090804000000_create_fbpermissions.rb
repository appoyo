class CreateFbpermissions < ActiveRecord::Migration
  def self.up
    create_table :fbpermissions, :force => true do |t|      
      t.timestamps 
      t.string   :facebook_id, :null => false
    end
  end

  def self.down
    drop_table :fbpermissions
  end
end
