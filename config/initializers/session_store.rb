# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_connect_tutorial_session',
  :secret      => '8a9a9b4d508dc1bb57d2f708e214947f2645cf96ee6f3ba8fc53cc235b881b082307f724c3aa752c6cb7d81e4e40687735b4a070c082874bc5e93c9653bb85cc'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
