development:
  adapter: postgresql
  encoding: unicode
  database: fb_connect_development
  pool: 5
  username: root
  password:

test:
  adapter: postgresql
  encoding: unicode
  database: fb_connect_test
  pool: 5
  username: root
  password:

production:
  adapter: postgresql
  encoding: unicode
  database: fb_connect_production
  pool: 5
  username: root
  password: