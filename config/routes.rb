ActionController::Routing::Routes.draw do |map|
 #a map.logout '/logout', :controller => 'sessions', :action => 'destroy'
 # map.login '/login', :controller => 'sessions', :action => 'new'
 # map.register '/register', :controller => 'users', :action => 'create'
 # map.signup '/signup', :controller => 'users', :action => 'new'
  
 # map.invitefriend '/invitefriend', :controller => 'users', :action => 'invitefriend'  
 # map.facebookapp '/facebookapp', :controller => 'users', :action => 'facebookapp'
 # map.fbstart '/start', :controller => 'page', :action => 'start'  
  map.business '/business', :controller => 'fbpage', :action => 'business'  
  map.privat '/privat', :controller => 'fbpage', :action => 'privat'  
  map.my '/my', :controller => 'fbpage', :action => 'my'  
  map.help '/help', :controller => 'fbpage', :action => 'help'  
  map.gallery '/gallery', :controller => 'fbpage', :action => 'gallery' 
  map.start '/start', :controller => 'fbpage', :action => 'start'   
  #map.processcallback '/processcallback', :controller => 'users', :action => 'processcallback'
  #map.localcallback '/localcallback', :controller => 'users', :action => 'localcallback'
  
 # map.permission '/permission', :controller => 'users', :action => 'permission'
  
  
  #map.publish_game_result '/publish_game_result', :controller => 'users', :action => 'publish_game_result'
 
  #map.statistic_to_csv '/statistic_to_csv', :controller => 'users', :action => 'statistic_to_csv'
  
  #map.api '/api', :controller => 'users', :action => 'api'
 
 
  #map.fakejawaker '/fakejawaker', :controller => 'users', :action => 'fakejawaker'
  map.connect '/connect', :controller => 'users', :action => 'home'
  
  #map.update '/update', :controller => 'friend', :action => 'update'
  map.stat '/stat', :controller => 'users', :action => 'stat'
  #map.signout '/signout', :controller => 'users', :action => 'sign_out'

  #map.auth '/auth', :controller => 'users', :action => 'auth'
  
  map.resources :users, :collection => {:link_user_accounts => :get}
  map.resource :session
   
  
  # The priority is based upon order of creation: first created -> highest priority.

  # Sample of regular route:
  #   map.connect 'products/:id', :controller => 'catalog', :action => 'view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   map.resources :products

  # Sample resource route with options:
  #   map.resources :products, :member => { :short => :get, :toggle => :post }, :collection => { :sold => :get }

  # Sample resource route with sub-resources:
  #   map.resources :products, :has_many => [ :comments, :sales ], :has_one => :seller
  
  # Sample resource route with more complex sub-resources
  #   map.resources :products do |products|
  #     products.resources :comments
  #     products.resources :sales, :collection => { :recent => :get }
  #   end

  # Sample resource route within a namespace:
  #   map.namespace :admin do |admin|
  #     # Directs /admin/products/* to Admin::ProductsController (app/controllers/admin/products_controller.rb)
  #     admin.resources :products
  #   end

  # You can have the root of your site routed with map.root -- just remember to delete public/index.html.
  # map.root :controller => "welcome"

  # See how all your routes lay out with "rake routes"

  # Install the default routes as the lowest priority.
  # Note: These default routes make all actions in every controller accessible via GET requests. You should
  # consider removing the them or commenting them out if you're using named routes and resources.

  
  map.root :controller => "fbpage", :action => "start"
  map.connect ':controller/:action/:id'
  map.connect ':controller/:action/:id.:format'
end
