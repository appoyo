class User < ActiveRecord::Base

  class << self
    attr_accessor :sessionkey, :sessionkey_time
  end


  def facebook_session
    @facebook_session ||= returning Facebooker::Session.create do |session|
      session.secure_with!(session_key,facebook_id,1.day.from_now)
    end
  end

end
