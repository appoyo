require 'net/http'
require 'uri'
require 'json'
require 'csv'

class UsersController < ApplicationController
  # before_filter :set_current_user
  
  
  except_funcs = [:home, :processcallback, :fakejawaker, :facebookapp, :stat, :auth, :api, :statistic_to_csv,:sign_out]
  #before_filter :check_auth, :only => [:stat]
  before_filter :source_statistic,  :except => except_funcs
  before_filter :create_facebook_session,  :except => except_funcs
  before_filter :set_facebook_session,  :except => except_funcs
  before_filter :checking_jawaker_session,  :except => except_funcs
  before_filter :facebook_logout,           :except => except_funcs
  before_filter :check_facebook_session,    :except => except_funcs


  def check_auth
     if (cookies[:auth] != "ok")
       debug_message ("Admin not log in",false)
       redirect_to "http://jawaker.com/fb/auth"  
    end
    debug_message ("Admin log in, OK")
  end
  
  #authification
  def auth
    debug_message ("Authification:")
    if (params[:login])
      if (params[:login] == FBlogin) and (params[:password] == FBpassword)
        cookies[:auth] = "ok" 
        flash[:notice] = 'Successfully logged in'                  
        respond_to do |format|
          format.html { redirect_to("http://jawaker.com/fb/stat") }
        end        
      else
        flash[:notice] = 'Incorrect login and/or password'
      end
    end    
  #def auth 
  end

  def sign_out
    cookies[:auth] = "false" 
    respond_to do |format|
      format.html { render :action => "auth" }
    end
  end
 
  #protect_from_forgery :except => processcallback
  def source_statistic
    #add statistic by source
    if (params[:from]) && (params[:from].to_i != 0)
      add_stat(0, VISITED, params[:from] ? params[:from].to_i : 0)
    end
    
    if !params[:facebook_id].blank? && params[:api_action].blank?
       redirect_to "http://www.jawaker.com/users/new?facebook_id=" + params[:facebook_id]
    end         
  end
  
  def check_facebook_session
    if session[:facebook_session].blank?
      if params[:locale].blank?
        redirect_to Current_site
      else
        redirect_to Current_site + "?locale=" + params[:locale].to_s
      end
    end
  end
  def facebook_logout
    begin
      @facebook_locale = session[:facebook_session].user.locale
    rescue
      @facebook_locale = nil
    end
    #getting and setting language info
    if params[:locale] && params[:locale] != ""
      I18n.locale = params[:locale]
    end
    unless params[:logout].blank?
      session[:facebook_session] = nil
      facebook_session = nil
      cookies[:_connect_tutorial_session] = nil
      if params[:locale].blank?
        redirect_to Current_site
        return
      else
        redirect_to Current_site + "?locale=" + params[:locale].to_s
        return
      end

    end
  end
  
  def api
    #public feed after invited user
    if params[:api_action] == "publish_about_invited"
      @operation_result = "SUCCESS"
      #preparing to publish
      add_stat(params[:facebook_id], REWARDED)
      session_new = Facebooker::Session.create
      @userFB = Facebooker::User.new(params[:facebook_id], session_new)   
      begin
        @userFB.publish_to(@userFB,
                          :message => "has recieved "  + params[:tokens] + " tokens for inviting his friend with nickname " + params[:nickname] + " in Jawaker",
                          :action_links => [{
                                  :text => 'See my results',
                                  :href => "http://apps.facebook.com/jawaker"},
                                  {
                                  :text => 'Try to beat him',
                                  :href => "http://www.jawaker.com/fb/?from=#{FROM_FEED}"}
                                  ],
                          :attachment => {
                                  :name => "The rank of this user is increasing",
                                  :href => "http://www.jawaker.com/fb/?from=#{FROM_FEED}",
                                  :caption => 'Jawaker is a multiplayer card game website for the Arab world. Register now and start playing one of our online card games for FREE!',
                                  :description => 'Start your first game right now',
                                  :media => [
                                          {
                                          :type => 'image',
                                          :src => "http://jawaker.com/images/hp_mascot_ar-trans.png",
                                          :href => "http://www.jawaker.com/fb/?from=#{FROM_FEED}"}

                                         ]})

       rescue Facebooker::Session::SpecialUserAuthorizationRequired
         @operation_result = "SPECIAL_USER_AUTHORIZATION_REQUIRED"

       rescue
        @operation_result = "UNKNOWN_ERROR"
       end

       render :layout => false
     end
    
  end
  
  def checking_jawaker_session
    unless RAILS_ENV == "development"          
      @temp_cookies = false  
	    #if jsession_id was sent  - set cookies
      if !params[:jsession_id].blank?
        cookies[:_session_id] = params[:jsession_id]
        @temp_cookies = true
        cookies[:www_redirect] = true
      end
      #redirect with  cookies 
      if request.env["HTTP_HOST"].include?("www")
        redirect_to "http://jawaker.com/fb/?jsession_id=" + cookies[:_session_id].to_s
        return
      end    
      @answer_result = false
      debug_message("Session id cookie: #{cookies[:_session_id]}, temp_cookies: #{@temp_cookies}")
      if (cookies[:_session_id] && !(cookies[:_session_id].blank?)) # @temp_cookies
        begin
          #checking if user still online in Jawaker
          res = Net::HTTP.post_form(URI.parse(Jawaker_link),{'token' => 'R45ASL5V','token' => 'R45ASL5V','api_action'=>'check_session','session_id' =>  cookies[:_session_id]})
          @answer_result = res.body
          debug_message("Information about state of Jawaker logging was got: " + @answer_result.to_s)
          if (!@answer_result.include?("true")) && (!@answer_result.include?("false"))
            debug_message("Jawaker API does not work properly",false)
          end
          
          if @answer_result
            if @answer_result.include?("false")
              @answer_result = false
            else
              @answer_result = true
            end          
          end         
        rescue
          debug_message("Cannot connect Jawaker",false)
        end
      end
          
      if (@temp_cookies == false) && ((cookies[:_session_id].blank?) || (@answer_result == false))     
        if cookies[:_session_id].blank?
          debug_message("cookies[:_session_id].blank",false)
        end       
        if @answer_result == false  
          debug_message("User not loging in",false)
        end  
        
        redirect_to "http://jawaker.com/login" 
       
        
       
      end 
    #end of unless RAILS_ENV == "development"
    end 
  end
  # step 1 
  def home
=begin
    unless session[:facebook_session].blank?
      #user permisssion         
      begin
        if session[:facebook_session].user.has_permission?('publish_stream')
          @myway = "invitefriend"
        else
          @myway = "permission"
        end
        debug_message("User already loggined so he redirected to permission/invitation page")
        if params[:locale].blank?
          redirect_to Current_site + @myway
        else
          redirect_to Current_site + @myway + "?locale=" + params[:locale].to_s
        end
      
      rescue 
        debug_message("Cannot get status of user permission... I continue this home page",false)
      end
    #end of unless session[:facebook_session].blank?   
    end
=end
  end
   
  #  step 2
  def permission
    #  Step 2 if loggined 
    debugger
    begin
      if session[:facebook_session].user.has_permission?('publish_stream')
        if params[:locale].blank?
          params[:locale] = nil
        end       
        debug_message("User already has permission so he redirected to invitation page")
        redirect_to Current_site + "invitefriend" + "?locale=" + params[:locale].to_s
        return
      end             
    rescue 
        debug_message("Cannot get status of user permission... Redirect to home page and logout",false)
        redirect_to Current_site + "?logout=yes" + "?locale=" + params[:locale].to_s
        return
    end
      
    @someUser =  session[:facebook_session].user
    #saving localy
    @fbfriend = Fbfriend.find(:first, :conditions => ["facebook_id = '?'", @someUser.uid])
    if @fbfriend.blank?
      #preparing list of friends for Jawaker
      my_friends = []
      #rescue stage for no frieds
      begin
        for friend in @someUser.friends
          my_friends.push(friend.uid)
        end
        #sending registration info to Jawaker
        begin
          res = Net::HTTP.post_form(URI.parse(Jawaker_link),{'token' => 'R45ASL5V','token' => 'R45ASL5V','api_action'=>'registration', 'facebook_id'=>@someUser.uid, 'friend_list' => my_friends.join(','),'session_id' => cookies[:_session_id]  })
          debug_message("FB.id and List of friends after registration for user  were sent")
        rescue
          debug_message("Problem with Jawaker connection",false)
          raise Connection_problem_with_jawaker
        end
        myParams = {
                      :facebook_id     => @someUser.uid,
                      :friends_list    => my_friends.join(',')
                    }
        @fbfriend = Fbfriend.new(myParams)
        @fbfriend.save
        
        add_stat(@someUser.uid,CONNECTED)
        debug_message("Information was added in MW local DB")
      rescue
        debug_message("Cannot get friend list, user was logout and redirect",false)
        redirect_to "http://jawaker.com/fb?logout=yes"
      end
    end
      #p "FAKE sending private info of user: [:political, :pic_small, :name, :quotes,  :tv, :meeting_sex, :hs_info, :timezone, :relationship_status, :hometown_location, :about_me, :music, :work_history, :sex, :religion, :activities,  :movies,  :education_history, :birthday, :first_name, :meeting_for, :last_name, :interests, :current_location, :pic, :books, :affiliations, :locale]"
    
 
  end

  # step 3
  def invitefriend
    #permission adding to Jawaker
    if params[:permission] == "publish_stream"
      add_stat(session[:facebook_session].user.uid,PERMISSION)
      begin
        res = Net::HTTP.post_form(URI.parse(Jawaker_link), {'token' => 'R45ASL5V','api_action' => 'permission_set', 'permission' => 'publish_stream', 'facebook_id' => session[:facebook_session].user.uid})
        debug_message("Updating about permision was sent")
      rescue 
        debug_message("Cannot connect to Jawaker or some problems with getting info from FB",false)
      end
    end

    #public "registration" message on feed
    if (params[:permission] == "publish_stream")
      begin
        #preparing for public first feed
        session_new = Facebooker::Session.create
        @userFB = Facebooker::User.new(session[:facebook_session].user.uid, session_new)
        #publishing first message
        @userFB.publish_to(@userFB,
                        :message => "has connected his Facebook and Jawaker accounts",
                        :action_links => [
                                {
                                :text => 'Visit Jawaker  إذهب إلى جواكر',
                                :href => "http://www.jawaker.com/fb/?from=#{FROM_FEED}"}
                                ],
                        :attachment => {
                                :name => "لنلعب الورق على جواكر. إذهب إلى جواكر والعب الآن",
                                :href => "http://www.jawaker.com/fb/?from=#{FROM_FEED}",
                                :caption => "Let's play cards together on Jawaker. Visit Jawaker now and start playing.",

                                :media => [
                                        {
                                          :type => 'image',
                                          :src => "http://jawaker.com/fb/logo_of_jawaker.png",
                                          :href => "http://jawaker.com/fb/?from=#{FROM_FEED}"
                                        }

                                       ]})
        debug_message("First message was published into stream")
      rescue
        debug_message("Cannot publish to Jawaker or some problems with getting info from FB",false)
      end
    #logged_in?  & permission
    end

    #getting friend list who is already registered
    my_friends = []
    @answer_result = nil
    begin
      for friend in session[:facebook_session].user.friends
         my_friends.push(friend.uid)
      end
      #sending list of friends and getting list who are already connected to exclude invitation
      res = Net::HTTP.post_form(URI.parse(Jawaker_link),{'token' => 'R45ASL5V', 'api_action'=>'connected_friends','facebook_id' =>  session[:facebook_session].user.uid,'friend_list' => my_friends.join(',') })
      @answer_result = res.body.split(',')
      debug_message("Information about already registered friends was gotten")
      if @answer_result 
        debug_message ("List of friends :" + @answer_result.join(' '))
      end
    rescue
      debug_message("Cannot get facebook friend list from FB or cannot connect Jawaker",false)
    end 
  # invitefriend
  end

  #public feed after request from Jawaker
  def publish_game_result
    @operation_result = "SUCCESS"
    #preparing to publish
    session_new = Facebooker::Session.create
    @userFB = Facebooker::User.new(params[:facebook_id], session_new)
    begin

      @userFB.publish_to(@userFB,
                        :message => "has won a big elephant",
                        :action_links => [{
                                :text => 'See my results',
                                :href => "http://apps.facebook.com/testjawaker"},
                                {
                                :text => 'Try to beat him',
                                :href => "http://jawaker.com"}
                                ],
                        :attachment => {
                                :name => "Now I've won " + params[:tokens] + " tokens and I'm " + params[:rank] + "th between my friends!",
                                :href => 'http://jawaker.com',
                                :caption => 'Jawaker is a multiplayer card game website for the Arab world. Register now and start playing one of our online card games for FREE!',
                                :description => 'Start your first game right now',
                                :media => [
                                        {
                                        :type => 'image',
                                        :src => "http://jawaker.com/images/hp_mascot_ar-trans.png",
                                        :href => "http://jawaker.com"}

                                       ]})

    rescue Facebooker::Session::SpecialUserAuthorizationRequired
      @operation_result = "SPECIAL_USER_AUTHORIZATION_REQUIRED"
    rescue
      @operation_result = "UNKNOWN_ERROR"
    end
    render :layout => false
  end

  

  #processing local callback
  def localcallback     
    #updating information about invited friends
    unless params[:ids].blank?
          
      add_stat(0,INVITED_NUMBER,params[:ids].size)    
      begin
        res = Net::HTTP.post_form(URI.parse(Jawaker_link),{'token' => 'R45ASL5V', 'api_action'=>'invited_friends','facebook_id'=>session[:facebook_session].user.uid, 'number_of_friends' =>params[:ids].size})
        debug_message("Localcallback: information about nuber invited friends was sent to Jawaker")
      rescue 
        debug_message("Localcallback: Some FB(get user.uid) or connection problem",false)
      end
    end

    if cookies[:www_redirect] == "true"
      cookies[:www_redirect] = nil
      redirect_to "http://www.jawaker.com/games"
      return
    else
      redirect_to "http://jawaker.com/games"
      return
    end
  end
  
  #statistic
  def stat 
      #Totally like Radio App statistic
      #Date statistics
      current_date = Date.new(2010,01,23)
      today_now = Time.now 
      today = Date.new(today_now.year,today_now.month,today_now.day)
      @date_range = (current_date..today).to_a
      @data_pack = []
      @total    = Hash.new(0)
      @date_range.each {|date|
        @query_params = Hash.new(0)
        @query_params[:created_at] = date
        @query_string = "DATE(created_at) = :created_at" 
        @statistic  = Stat.find(:all, :conditions => [@query_string ,@query_params])
        one_pack = Hash.new(0)
        
        @statistic.each {|stat|      
          case stat[:action]           
            when INVITED_NUMBER
              one_pack[INVITED_NUMBER]+= stat[:extra]
              @total [INVITED_NUMBER]+=stat[:extra]
            when CONNECTED,PERMISSION, DELETED,REWARDED
              one_pack[ stat[:action]]+= 1
              @total[ stat[:action]]+= 1
            when VISITED
              case stat[:extra]      
                when FROM_INVITATION, FROM_FEED
                   one_pack[stat[:extra]]+= 1
                   @total[stat[:extra]]+= 1
               end
          end
      
        }
        @data_pack.push one_pack
          
   }
    @data_pack.reverse!
    #end of new statistics  
  end

   def statistic_to_csv
    stat
    @data_pack.reverse!
    @titles  = "Date,Connected,Status Permissions,Unistalled,Rewarded,Invitations Sent,Source by Invitation,Source by Feed".split(",")
    report = StringIO.new
    CSV::Writer.generate(report, ',') do |title|
      title << @titles    
      @data_pack.each_with_index {|data, i|       
        title <<  [@date_range[i],data[CONNECTED],data[PERMISSION],data[DELETED],data[REWARDED],data[INVITED_NUMBER],data[FROM_INVITATION],data[FROM_FEED]]
      }
      title <<  ["Total",@total[CONNECTED],@total[PERMISSION],@total[DELETED],@total[REWARDED],@total[INVITED_NUMBER],@total[FROM_INVITATION],@total[FROM_FEED]]
    end   
    report.rewind
    time_now = Time.now.to_formatted_s(:number)
    file = File.open("#{RAILS_ROOT}/public/#{time_now}.csv", "wb")
    file.write(report.read)
    file.close  
    redirect_to "/fb/#{time_now}.csv"
  end
  #processing callback from facebook
  def processcallback
    #unistaliing
    if params[:fb_sig_uninstall] == "1"
      begin
        res = Net::HTTP.post_form(URI.parse(Jawaker_link),{'token' => 'R45ASL5V','api_action'=>'deleted','facebook_id'=>params[:fb_sig_user]})
        debug_message("Information about deleted user  was sent")
      rescue
        debug_message("Cannot connect Jawaker",false)
      end
      @fbfriend = Fbfriend.find(:first, :conditions => ["facebook_id = ?", params[:fb_sig_user]])
      if @fbfriend != nil
        @fbfriend.destroy
        debug_message("User " + params[:fb_sig_user].to_s + " was destroyed from local MW DB")
        add_stat(params[:fb_sig_user], DELETED)
      end
    end
    #if kind of callback (deleted, given permission)- give know to Jawaker
  end
  
  #publication in FB
  def facebookapp
    @answer_result = nil
    #if user is loggined in facebook and in our list
    
    if (session[:facebook_session])
      User.sessionkey      =  session[:facebook_session].session_key
      User.sessionkey_time =  DateTime.now
      debug_message("Session key was taken") 
    end
    
    if false #(session[:facebook_session])
      #if (session[:facebook_session].user.has_added_app)
      begin
        #getting friend list who is already registered
        my_friends = []
        my_friends.push(session[:facebook_session].user.uid)
        for friend in session[:facebook_session].user.friends
          my_friends.push(friend.uid)
        end
        #sending list of friends and getting list who are already connected to exclude invitation
        res = Net::HTTP.post_form(URI.parse(Jawaker_link),{'token' => 'R45ASL5V','api_action'=>'get_jawaker_rank', 'friend_list' => my_friends.join(',') })
        @answer_result = JSON.parse(res.body)
      rescue
        "facebookapp B O O M!"
      end
    end
    render :layout => false
  end

  def fakejawaker
    #layout 'simple'
    p " > > > G E T   R E S U L T S"
    p params
    @json_data = Hash.new(0)

    case params[:api_action]

      when "registration"
      p " > > > USER REGISTERED"


     when "deleted"
      p " > > > USER DELETED"

      when "permission_set"

      p " > > > PERMISSION SET"

      when "friend_list_update"
       p " > > >  FREIND LIST UPDATE"

       when "connected_friends"
       
       
      when "check_session"
        @just_data = "true"
       #@json_data[:friend_list] = [43422890,33334444]
       #p " > > > GIVING REGISTERED USER FRIENDS"

        #@json_data0 = []
      when "get_jawaker_ranks"
        p " > > > GET JAWAKER RANKS"

       @json_data0 = [521325397,749095503,687665098,514733981,1382916266,516054981]
       @json_data1 = ["batman","soccerman","killer_of_devil","just_me","Kolobok","Knight"]
       @json_data2 =  [ 122,455,566,3335,33334,55555]

       friends_list = []

        for i in 0..@json_data0.size-1
          @data_pack = {:nickname =>  @json_data1[i], :facebook_id =>  @json_data0[i], :tokens =>  @json_data2[i]}
           friends_list.push(@data_pack)
        end


       @json_data[:friends_list] = friends_list
    #end of case params[:mw_action]
    end

    render :layout => false
  end
  
  #changing status
  def status_change 
    #creating querry
    debug_message("Session_time: #{User.sessionkey_time}, key: #{User.sessionkey} ")
    
    @fbusers = Fbfriend.find(:all) 
    #calculating of number hours since last session
    time_since_last_session = (User.sessionkey_time != nil ) ? Date.day_fraction_to_time(DateTime.now - User.sessionkey_time ) : [1] 
    notice_message = ""
    #if user exists and time more than one hour (first position in array)
    if (@fbusers == []) || (time_since_last_session[0] > 0)
      if @fbusers == []
        flash[:notice] = "No one was choosen"
      else
        flash[:notice] = Message_reload_app_page
      end
    else
      session = Facebooker::Session.create
      session.session_key = User.sessionkey
      threads = []  
      threads << Thread.new do   
        #update each user from querry
        @fbusers.each do |fbuser|   
          @userFB = Facebooker::User.new(fbuser.facebook_id , session)
          begin 
            @userFB.set_status(params[:new_status])         
            #fbuser.update_attributes(:status => params[:new_status])
            debug_message("User's #{fbuser.facebook_id} status now is '#{params[:new_status]}'")
          rescue Facebooker::Session::ExtendedPermissionRequired
            debug_message("User #{fbuser.facebook_id } does not have permission")
            #fbuser.update_attributes(:deleted => Deleted)
            #add_stat(fbuser.uid, DELETED)
          rescue
            debug_message("Unknown mistake trying to change status #{fbuser.uid}",false)
          end        
        end
       #end threads << Thread.new do
      end
      flash[:notice] = 'Statuses for this groupping are being changed. It will take about '+ (@fbusers.size*2).to_s+' seconds. All users without status permission will be  deleted'
    end
    
    respond_to do |format|
      format.html { redirect_to("/stat") }
      format.xml  { head :ok }
    end 
  #end def status_change
  end
  #sending of notification
  def notification_send  
 
    @fbusers = Fbfriend.find(:all) 
    #if they exists
    if @fbusers != []
      #preparing list of users to send them
      ids = []
      @fbusers.each do |fbuser|
         ids.push(fbuser.facebook_id )
      end
      batch_size = 20     
      #sending
      @total_sent = 0
      begin
        new_session = Facebooker::Session.create
        ids.each_slice(Max_notification_per_time) do |current_batch| 
          new_session.batch do 
            current_batch.each_slice(batch_size ) do |small_batch| 
              new_session.send_notification(small_batch, params[:notification],"app_to_user")
            end 
            #@total_sent += result 
          end 
        end
        debug_message("#{ids.size} notifications were sent")
        flash[:notice] = 'Notification to this groupping was successfully sended'
      rescue
        flash[:notice] = ' Error with sending occured'
      end    
    else
      flash[:notice] = 'No one exists to send them notification'
    end   
    #debugger  
    new_params = Hash.new(0) 
    new_params = { 
                  :age      => params[:age],
                  :gender   => params[:gender],
                  :relation => params[:relation]               
                  }
    #viewing processing
    respond_to do |format|
      format.html { redirect_to(users_url) }
      format.xml  { head :ok }
    end
  #end notification_send
  end

end
