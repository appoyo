require 'net/http'
require 'uri'
require 'json'

class FbfriendController < ApplicationController
  # Be sure to include AuthenticationSystem in Application Controller instead
  
  #before_filter :set_current_user
 
  before_filter :create_facebook_session
  before_filter :set_facebook_session
 
  Connection_problem_with_jawaker = "Connection problem with jawaker"


  def update
 
    session_new = Facebooker::Session.create   
    #read all list of current users
    fbfriends_lists = Fbfriend.all  
    
    
    
    fbfriends_lists.each do |fbfriends_list|   
      
      @userFB = Facebooker::User.new(fbfriends_list.facebook_id, session_new)
      session_new.batch do 
        z = @userFB.friends()
      end
      
      begin 
        my_friends = []
        
        
          for friend in @userFB.friends
            my_friends.push(friend.uid)
          end
    
        
        new_list = my_friends.join(',')
        
        if  (new_list != fbfriends_list.friends_list)
    
           fbfriends_list.update_attributes(:friends_list => new_list)
      
           #update Jawaker
           begin
             res = Net::HTTP.post_form(URI.parse(Jawaker_link),{'api_action'=>'friend_list_update', 'facebook_id'=>fbfriends_list.facebook_id, 'friend_list' => new_list})
           rescue
             raise Connection_problem_with_jawaker        
           end  
        end
          
      rescue
        p "Unknown mistake"
      end 
    end   
    
    render :layout => false
  
  #update  
  end
  
end
